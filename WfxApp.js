import React, { Component } from 'react';
import {StyleSheet, Text, View, ScrollView, TouchableOpacity,
         FlatList,ActivityIndicator,SafeAreaView} from 'react-native';
import {DrawerNavigator, DrawerItems, StackNavigator,
        NavigationActions } from 'react-navigation'

import Icon from 'react-native-vector-icons/FontAwesome';

let registeredComponents= {};

function regComponent(component){
  registeredComponents[component.name] = component;
}

function removeComponent(name){
  registeredComponents.remove(name);
}

class PlaceHolderScreen extends Component {
  render() { return <Text>Place holder for Unregistered component.....</Text>}
}

function getComponent(name) {
  console.log("LOOOKINg for -------->>>>" + name);
  console.log(registeredComponents);
  let comp = registeredComponents[name];
  if(!comp){
    comp = PlaceHolderScreen;
  }
  return comp;
}

class DynamicStack extends Component {
  constructor(props) {
    super(props);
  }

  getNavItem(navItem) {
    var route = {screen: getComponent(navItem.screen)};
    return route;      
  }
  
  getNavItems(navItems) {
    console.log("nav dynamic stack items -------")
    return navItems.reduce((routes, navItem) => {
      routes[navItem.id] = this.getNavItem(navItem);
      return routes;
    }, {});
  }
  


  openMenu(){
    this.props.openDrawer();
  }

  render() {
    var Nav = StackNavigator(this.getNavItems(this.props.navItems),
                             {
                               navigationOptions: ({navigation}) =>
                                 ({
                                   headerRight :<Text style={styles.menu}
                                   onPress={() =>
                                            {(this.openMenu())}}>
                                     Menu
                                   </Text>})}
                            );
    return <Nav navItems={this.props.navItems}/>;      
  }
}

function mapItems(navigation, navItems){  
  if(navItems)
  {    
    return navItems.map(
      (route)=>
        {
          return <Text key={route.id}
          style={{padding: 5,
                  borderBottomColor:"grey",
                  borderBottomWidth: 1 }}
          onPress={()=> {
            navigation.navigate('DrawerClose');
            navigation.navigate(route.id,{navigation: navigation});           
          }}>
            {route.id}</Text>})}
  else {
    return {};
  }
}

class CustomDrawerContentComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {screenName: null};

  }
  render()  
  {
    return(
      <ScrollView style={styles.container}>
        <SafeAreaView style={styles.container}
                      forceInset={{ top: 'always', horizontal: 'never' }}>
          {mapItems(this.props.navigation, this.props.navItems)}
        </SafeAreaView>
      </ScrollView>)}
};

class MainMenu extends Component {
  constructor(props) {
    super(props);
  }

  openDrawer(){
    this.navigator && this.navigator.dispatch(
      NavigationActions.navigate({ routeName: 'DrawerOpen' }));

    }

  getNavItem(navItem) {
    
    const DynamicComponent = getComponent(navItem.screen);
    console.log("POOOOOOeS -- > " + DynamicComponent);
    return {screen: (props)=> <DynamicComponent
            toggleUser={this.props.toggleUser}
            openDrawer={()=>this.openDrawer()}
            navItems={navItem.items}/>}
    }
  
  getNavItems(navItems) {
    console.log("nav menuitems -------");
    console.log(navItems);
     console.log("-- nav menuitems -------");
    return navItems.reduce((routes, navItem) => {
      routes[navItem.id] = this.getNavItem(navItem);
      return routes;
    }, {});
  }

      
  
  render() {       
    var Nav = DrawerNavigator(this.getNavItems(this.props.navItems),
                              {drawerWidth: 300,
                               initialRouteParams: this.props.navItems,
                               contentComponent:
                               ({navigation}) =>
                               <CustomDrawerContentComponent
                               navigation={navigation}
                               navItems={this.props.navItems}/>
                              });

    return <Nav ref={(c)=>{this.navigator = c;}} navItems={this.props.navItems}/>
  }
}

class LoginScreen extends Component {
  constructor(props){
    super(props);    
  }

  render() {return <Text onPress={()=>{this.props.toggleUser("Piet Snot");}}>
            Login
            </Text> }
}

class LogoutScreen extends Component {
  constructor(props){
    super(props);
  }

  componentDidMount(){
    this.props.toggleUser(null);
  }
  
  render() { return <Text onPress={()=>{this.props.toggleUser(null)}} >
             Logging out....
             </Text> }
}



export class WfxApp extends Component {
  constructor(props) {
    super(props);

    this.state={isLoggedIn: false};
    
    regComponent(StackNavigator);
    regComponent(DynamicStack);
    regComponent(MainMenu);
    regComponent(PlaceHolderScreen);
    regComponent(LoginScreen);
    regComponent(LogoutScreen);

    if(this.props.components){
      this.props.components.map((comp)=>{
        console.log("reging *****---->" + comp)
        regComponent(comp);});
    }

    this.onUserChange = this.onUserChange.bind(this);
    }

  getMenu(){
    let menu = this.props.getMenu();
    menu.push({id: "Logout",
               screen: "LogoutScreen" })
    return menu;
  }
 
  onUserChange(user){
    if(user){
      this.setState({isLoggedIn: true});
    }
    else{
      this.setState({isLoggedIn: false});
    }
  }
  
  render() {
    let InitalComponent = getComponent(this.state.isLoggedIn?("MainMenu"):("LoginScreen"));

    return (
      <View style={styles.container}>
        <InitalComponent
          toggleUser={this.onUserChange}
          navItems={this.getMenu()}
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  menu: {
    padding: 5
  },
});
