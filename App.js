import React, { Component } from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import {WfxApp} from './WfxApp.js';

class MyScreenTest extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <View>
      <Text>Looking at MyScreenTest</Text>
      <Text style={{margin: 10}} 
    onPress={() => this.props.navigation.navigate('Screen2')}>
      Navigate To Screen2
    </Text>
      </View>}
}

class MyScreenTest2 extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <Text>Looking at MyScreenTest2</Text> }
}

function getMenu(){
  return  [{id: "Item1",
            screen: "DynamicStack",
            items: [
              {id: "Screen1",  screen: "MyScreenTest"} ,
              {id: "Screen2",  screen: "MyScreenTest2"}
            ]
           },];
}

export default class App extends Component{
  render(){
    return(
      <View style={styles.container}>
        <WfxApp
          components={[MyScreenTest,MyScreenTest2]}
          getMenu={getMenu}
          />
      </View>
    )}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});

